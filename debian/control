Source: openvas-scanner
Section: admin
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13),
               bison,
               cmake,
               pkgconf,
	       libcurl4-gnutls-dev,
               libglib2.0-dev,
	       libbsd-dev,
	       libjson-glib-dev,
               libgcrypt20-dev,
               libgnutls28-dev,
	       libpaho-mqtt-dev,
               libpcap-dev,
               libgvm-dev (>= 22.6.3),
               doxygen,
# Exclude those two architectures until cgreen is fixed:
# https://github.com/cgreen-devs/cgreen/issues/227
# https://github.com/cgreen-devs/cgreen/issues/239
               libcgreen1-dev [!ppc64el !s390x]
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://www.greenbone.net/
Vcs-Browser: https://salsa.debian.org/pkg-security-team/openvas-scanner
Vcs-Git: https://salsa.debian.org/pkg-security-team/openvas-scanner.git

Package: openvas-scanner
Section: net
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, openssl, redis-server,
         adduser, rsync, gpg
Replaces: openvas-server, openvas-plugins, libopenvas-dev, openvas-nasl
Conflicts: openvas-server, openvas-plugins, libopenvas-dev, openvas-nasl
Recommends: nmap, python3-impacket
Suggests: gvm-tools, snmp, pnscan, strobe, ike-scan
Description: remote network security auditor - scanner
 The Open Vulnerability Assessment System is a modular security auditing
 tool, used for testing remote systems for vulnerabilities that should be
 fixed.
 .
 It is made up of two parts: a scan server, and a client. The scanner/daemon,
 openvassd, is in charge of the attacks, whereas the client,
 gvm-tools, provides an X11/GTK+ user interface.
 .
 This package provides the scanner.
